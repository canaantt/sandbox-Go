// variadic.go

package main

import "fmt"

func simpleVariadicToSlice(numbers ...int) []int {
	return numbers
}

func mixedVariadicToSlice(name string, numbers ...int) (string, []int) {
	fmt.Println(numbers, "")
	return name, numbers
}

func sum(numbers ...int) int {
	fmt.Println(numbers, "")
	total := 0
	for _, num := range numbers {
		total += num
	}
	fmt.Println("total is: ", total)
	return total
}

func main() {
	mixedVariadicToSlice("hello", 1, 2, 3, 4, 5, 6)
	simpleVariadicToSlice(11, 12, 13, 14)
	sum(1, 2, 3, 4, 5, 6, 7)
}
