// variadic_test.go

package main

import (
	"fmt"
	"testing"
)

func TestSimpleVariadicToSlice(t *testing.T) {
	// Test for no arguments
	if val := simpleVariadicToSlice(); val != nil {
		t.Error("Value should be nil", nil)
	} else {
		t.Log("simpleVariadicToSlice() -> nil")
	}

	// Test for random set of values
	vals := simpleVariadicToSlice(1, 2, 3)
	expected := []int{1, 2, 3}
	isErr := false
	for i := 0; i < 3; i++ {
		if vals[i] != expected[i] {
			isErr = true
			break
		}
	}
	if isErr {
		t.Error("value should be []int{ 1, 2, 3}", vals)
	} else {
		t.Log("simpleVariadicToSlice() -> []int{ 1, 2, 3}")
	}

	// Test for a slice
	vals = simpleVariadicToSlice(expected...)
	isErr = false
	for i := 0; i < 3; i++ {
		if vals[i] != expected[i] {
			isErr = true
			break
		}
	}
	if isErr {
		t.Error("value should be []int{1, 2, 3}", vals)
	} else {
		t.Log("simplevariadicToSlice([int{1, 2, 3}...) -> []int{1, 2, 3}")
	}
}

func TestMixedVariadicToSlice(t *testing.T) {
	str, numbers := mixedVariadicToSlice("BB")
	if str == "BB" && numbers == nil {
		t.Log("Received as exepcted: BB, <nil slice>")
		fmt.Println("str is:", str)
	} else {
		t.Error("Received unexpectee values:", str, numbers)
	}
}
