package main

import (
	"errors"
	"fmt"
)

type User struct {
	Name, Email, Role string
	Age               int
}

func (u User) Salary() (int, error) {
	// I'm writing some comment
	switch u.Role {
	case "Engineer":
		return 100, nil
	case "Architect":
		return 200, nil
		// default:
		// 	return 0
	}
	return 0, errors.New(
		fmt.Sprintf("I'm not able to handle this '%s' role", u.Role),
	)
}

func (u *User) updateEmail(email string) {
	u.Email = email
}

func main() {
	jenny := User{Name: "Jenny", Email: "test", Role: "Engineer", Age: 38}
	john := User{Name: "John", Email: "test", Role: "Architect", Age: 48}
	if salary, err := jenny.Salary(); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Salary: ", salary)
	}
	fmt.Println(jenny.Salary())
	fmt.Println(john.Salary())
	jenny.updateEmail("Hi")
	fmt.Println(jenny)
}
