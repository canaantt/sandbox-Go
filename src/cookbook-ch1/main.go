package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
)

const info = `
Application %s starting, The binary was build by GO: %s`

func main() {
	log.Printf(info, "Example", runtime.Version())
	args := os.Args
	fmt.Println(args)
	programName := args[1]
	fmt.Printf("Ths binary name is: %s \n", programName)
}
