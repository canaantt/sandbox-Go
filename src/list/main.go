package main

type Node struct {
	value int
	next  *Node
	prev  *Node
}

type List struct {
	head *Node
	tail *Node
}

func (l *List) First() *Node {
	return l.head
}

func (l *List) Last() *Node {
	return l.tail
}

func (l *List) Push(val int) {
	n := &Node{value: val}
	if l.head == nil {
		l.head = n
	} else {
		l.tail.next = n
		n.prev = l.tail
	}
	l.tail = n
}

func (n *Node) Next() *Node {
	return n.next
}

func (n *Node) Previous() *Node {
	return n.prev
}

func main() {
	l := &List{}
	l.Push(1)
	l.Push(2)
	l.Push(3)
	println(l.head.value)
	n := l.First()
	for {
		println(n.value)
		n = n.Next()
		if n.next == nil {
			println(n.value)
			break
		}
	}
	n = l.Last()
	for {
		println(n.value)
		n = n.Previous()
		if n.prev == nil {
			println(n.value)
			break
		}
	}
}
