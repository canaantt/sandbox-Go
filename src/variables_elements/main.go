package main

import (
	"fmt"
)

func sum(x, y int) int {
	return x + y
}

func swap(x, y int) (int, int) {
	return y, x
}
func main() {
	// name = "Jenny Lou"
	// x := 5
	// y := 10.3
	println(sum(1, 2))
	a, b := swap(10, 20)
	println(a)
	println(b)
	sum := func(x, y int) int {
		return x + y
	}
	println(sum(1, 4))
	for i := 0; i < 10; i++ {
		if i > 5 {
			println(i)
		} else {
			println("Less than 5...")
		}
	}
	arr := []string{"a", "b", "c", "c"}
	println("Lenght of arr is: ", len(arr))
	arr = append(arr, "aa", "bb", "cc", "dd")
	println("Lenght of arr is: ", len(arr))
	for index, value := range arr {
		println("index is: ", index, "value is: ", value)
	}

	user := map[string]string{
		"Name":  "Jenny",
		"email": "test@",
		// "Age":   "38",
	}
	fmt.Println(user)
	fmt.Println(user["Name"])
	user["Age"] = "38"
	age, ok := user["Age"]
	if ok {
		fmt.Println(age)
	} else {
		fmt.Println("ERROR!")
	}
	for key, value := range user {
		fmt.Println("Key: ", key, "value: ", value)
	}
}
