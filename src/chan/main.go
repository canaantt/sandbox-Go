package main

import (
	"fmt"
	"math/rand"
	"time"
)

func process2(ch chan int) {
	n := rand.Intn(10)
	time.Sleep(time.Duration(n) * time.Millisecond)
	ch <- n
}

func main() {
	ch := make(chan int)
	go process2(ch)

	fmt.Println("Waiting for process")
	nch := <-ch
	fmt.Printf("Process took %dms\n", nch)
}
